CREATE SCHEMA if not exists sw1;
DROP table if exists sw1.users;
CREATE TABLE IF NOT EXISTS sw1.users (
    email varchar(25) primary key,
    name varchar(20),
    password varchar(32),
    access varchar(10)
);
