package ru.itis.jdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgresConnectionProvider {
    public static Connection getConnection() {
        try {
            Class.forName(PostgresDBConnectionDataHolder.DRIVER);
        } catch (ClassNotFoundException e) {
            //TODO Добавить логер
            System.out.println("Драйвер базы данных не найден");
        }

        try {
            return DriverManager.getConnection(
                    PostgresDBConnectionDataHolder.URL,
                    PostgresDBConnectionDataHolder.USERNAME,
                    PostgresDBConnectionDataHolder.PASSWORD);
        } catch (SQLException e) {
            //TODO логер
            throw new RuntimeException(e);
        }
    }
}
