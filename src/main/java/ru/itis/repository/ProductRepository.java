package ru.itis.repository;

import ru.itis.model.Product;
import ru.itis.model.User;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {
    List<Product> findAll();

    void save(Product product);

    void update(Product product);

    void delete(Long id);

    Optional<Product> findById(Long id);
}
