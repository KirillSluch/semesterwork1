package ru.itis.repository;

import ru.itis.model.Order;

import java.util.List;

public interface OrderRepository {
    List<Long> findProductsOfThisOrder(Long orderId);

    List<Order> findAllByUserInHistory(String userId);

    List<Order> findAllByUserActive(String userId);
    List<Order> findAllActive();

    void saveOrder(Order order);

    void updateMakeHistory(Long id);

    void deleteById(Long id);
}
