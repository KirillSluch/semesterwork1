package ru.itis.repository;

import ru.itis.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> findAll();
    List<String> findAllEmails();

    void save(User user);

    void update(User user);

    void delete(String email);
    Optional<User> findByEmail(String email);
}
