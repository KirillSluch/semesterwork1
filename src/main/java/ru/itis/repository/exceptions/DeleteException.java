package ru.itis.repository.exceptions;

public class DeleteException extends RuntimeException{
    public DeleteException(String message) {
        super(message);
    }
}
