package ru.itis.repository;

import java.util.List;

public interface BasketRepository {
    List<Long> findAll(String userId);

    void save(String userId, Long productId);

    void deleteProductByUserId(String userId);
    void deleteProductByUserAndProductId(String userId, Long productId);
    void  deleteProductByUserAndProductIdOne(String userId, Long productId);

}
