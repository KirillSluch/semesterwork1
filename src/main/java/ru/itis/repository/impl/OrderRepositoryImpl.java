package ru.itis.repository.impl;

import ru.itis.jdbc.PostgresConnectionProvider;
import ru.itis.model.Order;
import ru.itis.model.Product;
import ru.itis.repository.OrderRepository;
import ru.itis.repository.impl.util.Delete;
import ru.itis.repository.impl.util.FindAll;

import java.sql.*;
import java.util.List;
import java.util.function.Function;

public class OrderRepositoryImpl implements OrderRepository {
    //language=SQL
    private final String SQL_INSERT_ORDER = "INSERT INTO sw1.orders (user_id, status) " +
            "VALUES (?, ?)";

    //language=SQL
    private final String SQL_INSERT_ORDER_PRODUCT = "INSERT INTO sw1.order_product (order_id, product_id) " +
            "VALUES (?, ?)";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from sw1.orders where id = ?;" +
            "delete from sw1.order_product where order_id = ?;";

    //language=SQL
    private static final String SQL_SELECT_ALL_ACTIVE_BY_ID = "SELECT * FROM sw1.orders WHERE user_id = ? and status = 'ACTIVE'";

    //language=SQL
    private static final String SQL_SELECT_ALL_ACTIVE = "SELECT * FROM sw1.orders WHERE status = 'ACTIVE'";

    //language=SQL
    private static final String SQL_SELECT_ALL_HISTORY_BY_ID = "SELECT * FROM sw1.orders WHERE user_id = ? and status = 'HISTORY'";

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCT_OF_THIS_ORDER = "SELECT * FROM sw1.order_product WHERE order_id = ?";

    //language=SQL
    private final String SQL_UPDATE_ORDER_MAKE_HISTORY = "UPDATE sw1.orders " +
            "SET status = 'HISTORY'" +
            " WHERE id = ?";

    private static final Function<ResultSet, Order> orderMapper = row -> {

        try {
            Long id = row.getLong("id");
            String userId = row.getString("user_id");
            String status = row.getString("status");

            return Order.builder().id(id).userId(userId).status(status).build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    private static final Function<ResultSet, Long> orderProductMapper = row -> {

        try {
            return row.getLong("product_id");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    private final FindAll<Long> findAllLong = new FindAll<>();
    private final FindAll<Order> findAllOrder = new FindAll<>();


    @Override
    public List<Long> findProductsOfThisOrder(Long orderId) {
        return findAllLong.findAllByLongId(orderProductMapper, SQL_SELECT_ALL_PRODUCT_OF_THIS_ORDER, orderId);
    }

    @Override
    public List<Order> findAllByUserInHistory(String userId) {
        return findAllOrder.findAllByStringId(orderMapper, SQL_SELECT_ALL_HISTORY_BY_ID, userId);
    }

    @Override
    public List<Order> findAllByUserActive(String userId) {
        return findAllOrder.findAllByStringId(orderMapper, SQL_SELECT_ALL_ACTIVE_BY_ID, userId);
    }

    @Override
    public List<Order> findAllActive() {
        return findAllOrder.findAll(orderMapper, SQL_SELECT_ALL_ACTIVE);
    }

    private void saveOrderProduct(Long orderId, Long productId, PreparedStatement statementOrderProduct) throws SQLException {
        statementOrderProduct.setLong(1, orderId);
        statementOrderProduct.setLong(2, productId);

        statementOrderProduct.execute();
    }

    @Override
    public void saveOrder(Order order) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statementOrder = connection.prepareStatement(SQL_INSERT_ORDER, Statement.RETURN_GENERATED_KEYS);
             PreparedStatement statementOrderProduct = connection.prepareStatement(SQL_INSERT_ORDER_PRODUCT);) {

            connection.setAutoCommit(false);
            statementOrder.setString(1, order.getUserId());
            statementOrder.setString(2, order.getStatus());
            int affectedRows = statementOrder.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't save order");
            }

            ResultSet generatedKeys = statementOrder.getGeneratedKeys();

            if (generatedKeys.next()) {
                order.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't obtain generated key");
            }

            for (Product product : order.getProductList()) {
                saveOrderProduct(order.getId(), product.getId(), statementOrderProduct);
            }

            connection.commit();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void updateMakeHistory(Long id) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ORDER_MAKE_HISTORY)) {

            statement.setLong(1, id);

            statement.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(Long id) {
        Delete.deleteByLongId(SQL_DELETE_BY_ID, id);
    }
}
