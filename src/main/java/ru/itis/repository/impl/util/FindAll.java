package ru.itis.repository.impl.util;

import ru.itis.jdbc.PostgresConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class FindAll<T> {
    public List<T>  findAll(Function<ResultSet, T> mapper, String SQL_SELECT_ALL) {
        List<T> products = new ArrayList<>();

        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    products.add(mapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return products;
    }

    public List<T> findAllByStringId(Function<ResultSet, T> mapper, String SQL_SELECT_ALL, String id) {
        List<T> productsId = new ArrayList<>();

        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            statement.setString(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    productsId.add(mapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return productsId;
    }

    public List<T> findAllByLongId(Function<ResultSet, T> mapper, String SQL_SELECT_ALL, Long id) {
        List<T> productsId = new ArrayList<>();

        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    productsId.add(mapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return productsId;
    }
}
