package ru.itis.repository.impl.util;

import ru.itis.jdbc.PostgresConnectionProvider;
import ru.itis.repository.exceptions.DeleteException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Delete {
    public static void deleteByStringId(String SQL_DELETE_BY_ID, String email) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {

            statement.setString(1, email);

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new DeleteException("Can't delete");
            }

        } catch (SQLException e) {
            throw new DeleteException("Can't delete");
        }
    }

    public static void deleteByLongId(String SQL_DELETE_BY_ID, Long id) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {

            statement.setLong(1, id);

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new DeleteException("Can't delete");
            }

        } catch (SQLException e) {
            throw new DeleteException("Can't delete");
        }
    }

    public static void deleteByDifferentId(String SQL_DELETE_BY_ID, String userId, Long productId) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {

            statement.setString(1, userId);
            statement.setLong(2, productId);

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new DeleteException("Can't delete");
            }

        } catch (SQLException e) {
            throw new DeleteException("Can't delete");
        }
    }

}
