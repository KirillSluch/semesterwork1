package ru.itis.repository.impl;

import ru.itis.model.User;
import ru.itis.jdbc.PostgresConnectionProvider;
import ru.itis.repository.UserRepository;
import ru.itis.repository.impl.util.Delete;
import ru.itis.repository.impl.util.FindAll;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class UserRepositoryImpl implements UserRepository {

    //language=SQL
    private final String SQL_INSERT_USER = "INSERT INTO sw1.users (name, email, password, access) " +
            "VALUES (?, ?, ?, ?)";

    //language=SQL
    private final String SQL_SELECT_ALL_EMAILS = "SELECT email from sw1.users";

    //language=SQL
    private final String SQL_UPDATE_USER = "UPDATE sw1.users " +
            "SET name = ?, email = ?, password = ?, access = ?" +
            " WHERE email = ?";

    //language=SQL
    private static final String SQL_DELETE_BY_EMAIL = "delete from sw1.users where email = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM sw1.users";

    //language=SQL
    private static final String SQL_SELECT_USER_BY_EMAIL = "SELECT * FROM sw1.users where email = ?";


    private static final Function<ResultSet, User> userMapper = row -> {

        try {
            String name = row.getString("name");
            String email = row.getString("email");
            String password = row.getString("password");
            String access = row.getString("access");
            return new User(name, email, password, access);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    @Override
    public List<User> findAll() {
        FindAll<User> findAll = new FindAll<>();

        return findAll.findAll(userMapper, SQL_SELECT_ALL);
    }

    @Override
    public List<String> findAllEmails() {
        List<String> emailList = new ArrayList<>();
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_EMAILS)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    emailList.add(resultSet.getString("email"));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return emailList;
    }

    @Override
    public void save(User user) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER)) {

            statement.setString(1, user.getName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getAccess());
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void update(User user) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER)) {

            statement.setString(1, user.getName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getAccess());
            statement.setString(5, user.getEmail());
            statement.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(String email) {
        Delete.deleteByStringId(SQL_DELETE_BY_EMAIL, email);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER_BY_EMAIL)) {

            statement.setString(1, email);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    User user = userMapper.apply(resultSet);
                    return Optional.of(user);
                } else {
                    return Optional.empty();
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
