package ru.itis.repository.impl;

import ru.itis.jdbc.PostgresConnectionProvider;
import ru.itis.model.Product;
import ru.itis.repository.ProductRepository;
import ru.itis.repository.impl.util.Delete;
import ru.itis.repository.impl.util.FindAll;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class ProductRepositoryImpl implements ProductRepository {
    //language=SQL
    private final String SQL_INSERT_PRODUCT = "INSERT INTO sw1.product (name, description, price, path_to_logo) " +
            "VALUES (?, ?, ?, ?)";


    //language=SQL
    private final String SQL_UPDATE_PRODUCT = "UPDATE sw1.product " +
            "SET name = ?, description = ?, price = ?, path_to_logo = ?" +
            " WHERE id = ?";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from sw1.product where id = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM sw1.product";

    //language=SQL
    private static final String SQL_SELECT_PRODUCT_BY_ID = "SELECT * FROM sw1.product where id = ?";


    private static final Function<ResultSet, Product> productMapper = row -> {

        try {
            Long id = row.getLong("id");
            String name = row.getString("name");
            String description = row.getString("description");
            float price = row.getFloat("price");
            String pathToLogo = row.getString("path_to_logo");
            return new Product(id, name, description, price, pathToLogo);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    @Override
    public List<Product> findAll() {
        FindAll<Product> findAll = new FindAll<>();

        return findAll.findAll(productMapper, SQL_SELECT_ALL);
    }

    @Override
    public void save(Product product) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PRODUCT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, product.getName());
            statement.setString(2, product.getDescription());
            statement.setFloat(3, product.getPrice());
            statement.setString(4, product.getPathToLogo());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't save user");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                product.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't obtain generated key");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PRODUCT)) {

            statement.setString(1, product.getName());
            statement.setString(2, product.getDescription());
            statement.setFloat(3, product.getPrice());
            statement.setString(4, product.getPathToLogo());
            statement.setLong(5, product.getId());

            statement.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Long id) {
        Delete.deleteByLongId(SQL_DELETE_BY_ID, id);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_PRODUCT_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Product product = productMapper.apply(resultSet);
                    return Optional.of(product);
                } else {
                    return Optional.empty();
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
