package ru.itis.repository.impl;

import ru.itis.jdbc.PostgresConnectionProvider;
import ru.itis.repository.BasketRepository;
import ru.itis.repository.impl.util.Delete;
import ru.itis.repository.impl.util.FindAll;

import java.sql.*;
import java.util.List;
import java.util.function.Function;

public class BasketRepositoryImpl implements BasketRepository {
    //language=SQL
    private final String SQL_INSERT_PRODUCT = "INSERT INTO sw1.basket (user_id, product_id) " +
            "VALUES (?, ?)";


    //language=SQL
    private static final String SQL_DELETE_BY_USER_ID = "delete from sw1.basket where user_id = ?";

    //language=SQL
    private static final String SQL_DELETE_BY_BOTH_ID = "delete from sw1.basket where user_id = ? and product_id = ?";


    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM sw1.basket WHERE user_id = ?";

    private static final Function<ResultSet, Long> productIdMapper = row -> {

        try {
            return (Long) row.getLong("product_id");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    @Override
    public List<Long> findAll(String userId) {
        FindAll<Long> findAll = new FindAll<>();

        return findAll.findAllByStringId(productIdMapper, SQL_SELECT_ALL, userId);
    }

    @Override
    public void save(String userId, Long productId) {
        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PRODUCT)) {

            statement.setString(1, userId);
            statement.setLong(2, productId);

            statement.execute();


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteProductByUserId(String userId) {
        Delete.deleteByStringId(SQL_DELETE_BY_USER_ID, userId);
    }

    @Override
    public void deleteProductByUserAndProductId(String userId, Long productId) {
        Delete.deleteByDifferentId(SQL_DELETE_BY_BOTH_ID, userId, productId);
    }

    @Override
    public void deleteProductByUserAndProductIdOne(String userId, Long productId) {
        Delete.deleteByDifferentId(SQL_DELETE_BY_BOTH_ID, userId, productId);
    }
}
