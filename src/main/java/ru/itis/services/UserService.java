package ru.itis.services;

import ru.itis.model.User;

import java.util.List;

public interface UserService {
    User signUp(String name, String email, String password);
    User logIn(String email, String password);
    List<String> getListOfUsersEmails();
    void makeAdmin(String email);
}
