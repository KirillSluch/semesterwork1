package ru.itis.services;

import java.util.List;

public interface BasketService {
    boolean delete(String userId, Long productId);

    boolean delete(String userId);

    boolean save(String userId, Long productId);
    List<Long> getAll(String userId);
}
