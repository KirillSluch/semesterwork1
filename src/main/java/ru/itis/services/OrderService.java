package ru.itis.services;

import ru.itis.model.Order;

import java.util.List;

public interface OrderService {

    boolean delete(Long orderId);

    boolean save(Order order);

    List<Order> getAllActiveById(String userId);
    List<Order> getAllActive ();
    void makeHistory(Long id);

    List<Order> getAllHistoryById(String userId);


}
