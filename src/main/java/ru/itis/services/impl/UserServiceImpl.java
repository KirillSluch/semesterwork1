package ru.itis.services.impl;

import org.apache.commons.codec.digest.DigestUtils;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;
import ru.itis.services.UserService;
import ru.itis.services.exceptions.UserAlreadyExistsException;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User signUp(String name, String email, String password) {
        List<String> inDBEmails = userRepository.findAllEmails();
        if (!inDBEmails.contains(email)){
            User user = User.builder()
                    .name(name)
                    .email(email)
                    .password(DigestUtils.md5Hex(password))
                    .access("USER")
                    .build();
            userRepository.save(user);
            return user;
        } else {
            throw new UserAlreadyExistsException("User Already Exists!");
        }
    }

    @Override
    public User logIn(String email, String password) {
        Optional<User> toCheck = userRepository.findByEmail(email);
        String md5Hex = DigestUtils.md5Hex(password);
        if (toCheck.isPresent()) {
            User user = toCheck.get();
            if (user.getPassword().equals(md5Hex)) {
                return user;
            } else {
                throw new IllegalArgumentException("Incorrect Password!");
            }
        } else {
            throw new IllegalArgumentException("User With This Email Not Exists!");
        }
    }

    @Override
    public List<String> getListOfUsersEmails() {
        return userRepository.findAllEmails();
    }

    @Override
    public void makeAdmin(String email) {
        Optional<User> toCheck = userRepository.findByEmail(email);
        if (toCheck.isPresent()) {
            User user = toCheck.get();
            user.setAccess("ADMIN");
            userRepository.update(user);
        } else {
            throw new IllegalArgumentException("User With This Email Not Exists!");
        }
    }
}
