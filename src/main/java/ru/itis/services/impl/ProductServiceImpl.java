package ru.itis.services.impl;

import ru.itis.model.Product;
import ru.itis.repository.ProductRepository;
import ru.itis.repository.exceptions.DeleteException;
import ru.itis.repository.impl.ProductRepositoryImpl;
import ru.itis.services.ProductService;

import java.util.List;
import java.util.Optional;

public class ProductServiceImpl implements ProductService {
    ProductRepository productRepository;
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public boolean delete(Long id) {
        try {
            productRepository.delete(id);
            return true;
        } catch (DeleteException e) {
            return false;
        }
    }

    @Override
    public boolean update(Product product) {
        try {
            productRepository.update(product);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Override
    public boolean create(Product product) {
        try {
            productRepository.save(product);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProductById(Long id) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        return optionalProduct.orElse(null);
    }
}
