package ru.itis.services.impl;

import ru.itis.repository.BasketRepository;
import ru.itis.repository.exceptions.DeleteException;
import ru.itis.services.BasketService;

import java.util.List;

public class BasketServiceImpl implements BasketService {
    BasketRepository basketRepository;

    public BasketServiceImpl(BasketRepository basketRepository) {
        this.basketRepository = basketRepository;
    }

    @Override
    public boolean delete(String userId) {
        try {
            basketRepository.deleteProductByUserId(userId);
            return true;
        } catch (DeleteException e) {
            return false;
        }

    }

    @Override
    public boolean delete(String userId, Long productId) {
        try {
            basketRepository.deleteProductByUserAndProductId(userId, productId);
            return true;
        } catch (DeleteException e) {
            return false;
        }
    }

    @Override
    public boolean save(String userId, Long productId) {
        try {
            basketRepository.save(userId, productId);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Override
    public List<Long> getAll(String userId) {
        return basketRepository.findAll(userId);
    }
}
