package ru.itis.services.impl;

import ru.itis.model.Order;
import ru.itis.model.Product;
import ru.itis.repository.OrderRepository;
import ru.itis.repository.ProductRepository;
import ru.itis.repository.exceptions.DeleteException;
import ru.itis.services.OrderService;

import java.util.ArrayList;
import java.util.List;

public class OrderServiceImpl implements OrderService {

    OrderRepository orderRepository;
    ProductRepository productRepository;

    public OrderServiceImpl(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @Override
    public boolean delete(Long orderId) {
        try {
            orderRepository.deleteById(orderId);
            return true;
        } catch (DeleteException e) {
            return false;
        }
    }

    @Override
    public boolean save(Order order) {
        try {
            orderRepository.saveOrder(order);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Override
    public List<Order> getAllActiveById(String userId) {
        try {
            List<Order> orderList = orderRepository.findAllByUserActive(userId);

            return setProductsToOrder(orderList);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public List<Order> getAllActive() {
        try {
            List<Order> orderList = orderRepository.findAllActive();

            return setProductsToOrder(orderList);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public void makeHistory(Long id) {
        orderRepository.updateMakeHistory(id);

    }

    private List<Order> setProductsToOrder(List<Order> orderList) {
        for (Order order : orderList) {
            List<Long> ids = orderRepository.findProductsOfThisOrder(order.getId());
            List<Product> productList = new ArrayList<>();
            for (Long id : ids) {
                productList.add(productRepository.findById(id).orElse(null));
            }
            order.setProductList(productList);
        }
        return orderList;
    }

    @Override
    public List<Order> getAllHistoryById(String userId) {
        try {
            List<Order> orderList = orderRepository.findAllByUserInHistory(userId);

            return setProductsToOrder(orderList);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
