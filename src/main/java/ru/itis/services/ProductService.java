package ru.itis.services;

import ru.itis.model.Product;

import java.util.List;

public interface ProductService {
    boolean delete(Long id);
    boolean update(Product product);
    boolean create(Product product);
    List<Product> getAllProducts();
    Product getProductById(Long id);

}
