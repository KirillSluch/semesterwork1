package ru.itis.servlets.content;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import ru.itis.model.Product;
import ru.itis.repository.ProductRepository;
import ru.itis.repository.impl.ProductRepositoryImpl;
import ru.itis.services.ProductService;
import ru.itis.services.impl.ProductServiceImpl;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@WebServlet(name = "ShopAdminServlet", value = "/shop/admin")
@MultipartConfig
public class ShopAdminServlet extends HttpServlet {
    private ProductService productService;
    @Override
    public void init() {
        this.productService = new ProductServiceImpl(new ProductRepositoryImpl());
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("productList", productService.getAllProducts());
        session.setAttribute("metaList", Product.getMeta());
        request.getRequestDispatcher("/WEB-INF/jsp/shopAdmin.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String[] toDeleteArray = request.getParameterValues("delete[]");

        if (toDeleteArray != null) {
            for (String idToDelete : toDeleteArray) {
                if (!productService.delete(Long.parseLong(idToDelete))) {
                    session.setAttribute("errors", "Can't delete!");
                }
            }
        } else if (request.getParameter("id") != null) {
            Product product = Product.builder().
                    id(Long.valueOf(request.getParameter("id"))).
                    name(request.getParameter("name")).
                    description(request.getParameter("description")).
                    price(Float.parseFloat(request.getParameter("price"))).
                    build();


            extractImage(request, product, "image");
            boolean ch = productService.update(product);
            if (!ch) {
                session.setAttribute("errors", "Can't update!");
            }
        } else if (request.getParameter("name") != null) {
            Product product = Product.builder().
                    name(request.getParameter("name")).
                    description(request.getParameter("description")).
                    price(Float.parseFloat(request.getParameter("price"))).
                    build();
            boolean ch = productService.create(product);

            extractImage(request, product, "image");

            ch &= productService.update(product);

            if (!ch) {
                session.setAttribute("errors", "Can't create!");
            }
        }
        response.sendRedirect(request.getContextPath() + "/shop/admin");
    }

    private void extractImage(HttpServletRequest request, Product product, String partName) throws IOException, ServletException {
        Part filePart = request.getPart(partName);

        String fileExtension = Paths.get(filePart.getSubmittedFileName()).getFileName().toString().split("\\.")[1];
        InputStream fileContent = filePart.getInputStream();
        BufferedImage image = ImageIO.read(fileContent);
        String contPath = "../images/" + product.getId() + "." + fileExtension;
        File file = new File(contPath);
        ImageIO.write(image, fileExtension, file);
        product.setPathToLogo(product.getId() + "." + fileExtension);
        System.out.println(product.getId() + "." + fileExtension);
    }
}
