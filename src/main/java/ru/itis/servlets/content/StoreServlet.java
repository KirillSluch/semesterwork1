package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.model.Product;
import ru.itis.repository.impl.ProductRepositoryImpl;
import ru.itis.services.ProductService;
import ru.itis.services.impl.ProductServiceImpl;

import java.io.IOException;
import java.util.Arrays;

@WebServlet(name = "ShopServlet", value = "/shop/store")
public class StoreServlet extends HttpServlet {
    private ProductService productService;
    @Override
    public void init() {
        this.productService = new ProductServiceImpl(new ProductRepositoryImpl());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        session.setAttribute("productList", productService.getAllProducts());
        request.getRequestDispatcher("/WEB-INF/jsp/store.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (request.getParameterValues("inOrder[]") != null) {
            session.setAttribute("basket", request.getParameter("inOrder[]"));
            response.sendRedirect(request.getContextPath() + "/shop/basket");
        } else {
            response.sendRedirect(request.getContextPath() + "/shop/store");
        }
    }
}
