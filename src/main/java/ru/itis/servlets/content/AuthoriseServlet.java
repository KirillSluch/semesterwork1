package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.model.User;

import java.io.IOException;

@WebServlet(name = "AuthoriseServlet", value = "/shop")
public class AuthoriseServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user.getAccess().equals("USER")) {
            response.sendRedirect(request.getContextPath() + "/shop/store");
        } else if (user.getAccess().equals("ADMIN")) {
            response.sendRedirect(request.getContextPath() + "/shop/admin");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
