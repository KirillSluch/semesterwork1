package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;
import ru.itis.repository.impl.UserRepositoryImpl;
import ru.itis.services.exceptions.UserAlreadyExistsException;
import ru.itis.services.UserService;
import ru.itis.services.impl.UserServiceImpl;

import java.io.IOException;

@WebServlet(name = "RegisterServlet", value = "/register")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/jsp/register.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserRepository userRepository = new UserRepositoryImpl();
        UserService userService = new UserServiceImpl(userRepository);
        try {
            User user = userService.signUp(
                    request.getParameter("name"),
                    request.getParameter("email"),
                    request.getParameter("password"));

            session.setAttribute("user", user);
            response.sendRedirect(request.getContextPath() + "/shop");
        } catch (UserAlreadyExistsException e) {
            session.setAttribute("errors", e.getMessage());
            response.sendRedirect(request.getContextPath() + "/register");
        }
    }
}
