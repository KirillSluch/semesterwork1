package ru.itis.servlets.content.utils;

import ru.itis.model.Order;
import ru.itis.model.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductsOfOrderPresenter {
    public static Map<Long, Map<Product, Integer>> makePresent(List<Order> activeOrderList) {
        Map<Long, Map<Product, Integer>> productsWithOrderId = new HashMap<>();
        for(Order order : activeOrderList) {

            Map<Product, Integer> products = new HashMap<>();

            for (Product product : order.getProductList()) {
                if (products.containsKey(product)) {
                    int numOfId = products.get(product) + 1;
                    products.replace(product, numOfId);
                } else {
                    products.put(product, 1);
                }
            }

            productsWithOrderId.put(order.getId(), products);

        }
        return productsWithOrderId;
    }
}
