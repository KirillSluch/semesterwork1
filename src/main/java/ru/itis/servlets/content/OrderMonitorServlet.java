package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.model.Order;
import ru.itis.model.Product;
import ru.itis.model.User;
import ru.itis.repository.ProductRepository;
import ru.itis.repository.impl.OrderRepositoryImpl;
import ru.itis.repository.impl.ProductRepositoryImpl;
import ru.itis.services.OrderService;
import ru.itis.services.ProductService;
import ru.itis.services.impl.OrderServiceImpl;
import ru.itis.services.impl.ProductServiceImpl;
import ru.itis.servlets.content.utils.ProductsOfOrderPresenter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "OrderMonitorServlet", value = "/shop/store/orderMonitor")
public class OrderMonitorServlet extends HttpServlet {
    private ProductService productService;
    private OrderService orderService;

    @Override
    public void init() throws ServletException {
        ProductRepository productRepository = new ProductRepositoryImpl();

        productService = new ProductServiceImpl(productRepository);
        orderService = new OrderServiceImpl(new OrderRepositoryImpl(), productRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userId = ((User) session.getAttribute("user")).getEmail();

        List<Order> activeOrderList = orderService.getAllActiveById(userId);
        List<Order> historyOrderList = orderService.getAllHistoryById(userId);

        Map<Long, Map<Product, Integer>> activeOrderProductsMapList = null;
        Map<Long, Map<Product, Integer>> historyOrderProductsMapList = null;


        if (activeOrderList != null) {
            activeOrderProductsMapList = ProductsOfOrderPresenter.makePresent(activeOrderList);
        }

        if (historyOrderList != null) {
            historyOrderProductsMapList = ProductsOfOrderPresenter.makePresent(historyOrderList);
        }

        session.setAttribute("active", activeOrderProductsMapList);
        session.setAttribute("history", historyOrderProductsMapList);

        request.getRequestDispatcher("/WEB-INF/jsp/orderMonitor.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/shop/store/orderMonitor");
    }
}
