package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.repository.impl.UserRepositoryImpl;
import ru.itis.services.UserService;
import ru.itis.services.impl.UserServiceImpl;

import java.io.IOException;

@WebServlet(name = "UsersAdminServlet", value = "/shop/admin/user")
public class UsersAdminServlet extends HttpServlet {
    UserService userService;

    @Override
    public void init() throws ServletException {
        this.userService = new UserServiceImpl(new UserRepositoryImpl());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/userAdmin.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("email") != null) {
            userService.makeAdmin(request.getParameter("email"));
        }

        response.sendRedirect(request.getContextPath() + "/shop/admin/user");
    }
}
