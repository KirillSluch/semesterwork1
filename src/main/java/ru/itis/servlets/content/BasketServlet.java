package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.model.Order;
import ru.itis.model.Product;
import ru.itis.model.User;
import ru.itis.repository.BasketRepository;
import ru.itis.repository.ProductRepository;
import ru.itis.repository.impl.BasketRepositoryImpl;
import ru.itis.repository.impl.OrderRepositoryImpl;
import ru.itis.repository.impl.ProductRepositoryImpl;
import ru.itis.services.BasketService;
import ru.itis.services.OrderService;
import ru.itis.services.ProductService;
import ru.itis.services.impl.BasketServiceImpl;
import ru.itis.services.impl.OrderServiceImpl;
import ru.itis.services.impl.ProductServiceImpl;

import javax.sound.sampled.Port;
import java.io.IOException;
import java.util.*;

@WebServlet(name = "BasketServlet", value = "/shop/basket")
public class BasketServlet extends HttpServlet {
    private ProductService productService;
    private BasketService basketService;
    private OrderService orderService;

    @Override
    public void init() throws ServletException {
        ProductRepository productRepository = new ProductRepositoryImpl();

        productService = new ProductServiceImpl(productRepository);
        basketService = new BasketServiceImpl(new BasketRepositoryImpl());
        orderService = new OrderServiceImpl(new OrderRepositoryImpl(), productRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Map<Product, Integer> products = new HashMap<>();
        String userId = ((User) session.getAttribute("user")).getEmail();

        if (session.getAttribute("basket") != null && !((String) session.getAttribute("basket")).equals("")) {
            String[] ids = ((String) session.getAttribute("basket")).trim().split(" ");
            for (String id : ids) {
                basketService.save(userId, Long.parseLong(id));
            }
        }
        session.setAttribute("basket", "");

        List<Long> productIdList = basketService.getAll(userId);

        if (productIdList != null) {
            for(Long id : productIdList) {

                Product product = productService.getProductById(id);
                if (products.containsKey(product)) {
                    int numOfId = products.get(product) + 1;
                    products.replace(product, numOfId);
                } else {
                    products.put(product, 1);
                }
            }
        }
        session.setAttribute("inBasketMap", products);

        request.getRequestDispatcher("/WEB-INF/jsp/basket.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userId = ((User) session.getAttribute("user")).getEmail();

        List<Long> productIdList = basketService.getAll(userId);
        String productToDelete = request.getParameter("productToDelete");


        if (productToDelete != null) {
            basketService.delete(userId, Long.valueOf(productToDelete));
            response.sendRedirect(request.getContextPath() + "/shop/basket");
        } else if (productIdList.size() != 0) {
            basketService.delete(userId);

            List<Product> productList = new ArrayList<>();

            for (Long id : productIdList) {
                productList.add(productService.getProductById(id));
            }

            orderService.save(Order.builder()
                    .userId(userId)
                    .status("ACTIVE")
                    .productList(productList)
                    .build());
            response.sendRedirect(request.getContextPath() + "/shop/store/orderMonitor");
        } else {
            response.sendRedirect(request.getContextPath() + "/shop/basket");
        }


    }
}
