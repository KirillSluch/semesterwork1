package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.model.User;
import ru.itis.repository.UserRepository;
import ru.itis.repository.impl.UserRepositoryImpl;
import ru.itis.services.UserService;
import ru.itis.services.impl.UserServiceImpl;

import java.io.IOException;

@WebServlet(name = "LogInServlet", value = "/login")
public class LogInServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (request.getParameter("check") != null && request.getParameter("check").equals("drop")) {
            session.setAttribute("user", null);
        }
        request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        try {
            UserRepository userRepository = new UserRepositoryImpl();
            UserService userService = new UserServiceImpl(userRepository);

            User user = userService.logIn(request.getParameter("email"), request.getParameter("password"));

            session.setAttribute("user", user);

            response.sendRedirect(request.getContextPath() + "/shop");
        } catch (IllegalArgumentException e) {
            session.setAttribute("errors", e.getMessage());

            response.sendRedirect(request.getContextPath() + "/login");
        }


    }
}
