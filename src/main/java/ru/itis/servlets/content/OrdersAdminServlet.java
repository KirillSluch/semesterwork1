package ru.itis.servlets.content;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.model.Order;
import ru.itis.model.Product;
import ru.itis.repository.impl.OrderRepositoryImpl;
import ru.itis.repository.impl.ProductRepositoryImpl;
import ru.itis.services.OrderService;
import ru.itis.services.impl.OrderServiceImpl;
import ru.itis.servlets.content.utils.ProductsOfOrderPresenter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "OrdersAdminServlet", value = "/shop/admin/orders")
public class OrdersAdminServlet extends HttpServlet {
    OrderService orderService;

    @Override
    public void init() throws ServletException {
        orderService = new OrderServiceImpl(new OrderRepositoryImpl(), new ProductRepositoryImpl());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        List<Order> activeOrderList = orderService.getAllActive();

        Map<Long, Map<Product, Integer>> activeOrderProductsMapList = null;

        if (activeOrderList != null) {
            activeOrderProductsMapList = ProductsOfOrderPresenter.makePresent(activeOrderList);
        }

        session.setAttribute("activeAdmin", activeOrderProductsMapList);

        request.getRequestDispatcher("/WEB-INF/jsp/ordersAdmin.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idToHistory = request.getParameter("idToHistory");

        if (idToHistory != null) {
            orderService.makeHistory(Long.parseLong(idToHistory));
        }

        response.sendRedirect(request.getContextPath() + "/shop/admin/orders");
    }
}
