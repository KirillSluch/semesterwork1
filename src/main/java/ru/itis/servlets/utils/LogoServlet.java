package ru.itis.servlets.utils;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.apache.taglibs.standard.extra.spath.Path;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;

@WebServlet(name = "LogoServlet", value = "/images/*")
public class LogoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        String imageName = request.getPathInfo().substring(1);
        try {
            InputStream imageStream = new FileInputStream("../images/" + imageName);
            byte[] content = imageStream.readAllBytes();
            response.setContentType(getServletContext().getMimeType(imageName));
            response.setContentLength(content.length);
            response.getOutputStream().write(content);
        } catch (IOException e) {
            throw new RuntimeException();
        }

    }

}
