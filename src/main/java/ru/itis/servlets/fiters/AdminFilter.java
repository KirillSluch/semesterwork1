package ru.itis.servlets.fiters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import ru.itis.model.User;

import java.io.IOException;

@WebFilter(filterName = "AdminFilter", urlPatterns = "/shop/admin/*")
public class AdminFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpSession session = httpServletRequest.getSession();

        if (session.getAttribute("user") != null && ((User) session.getAttribute("user")).getAccess().equals("ADMIN")) {
            chain.doFilter(request, response);
        } else {
            session.setAttribute("errors", "У вас нет прав доступа");
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/shop/store");
        }

    }
}
