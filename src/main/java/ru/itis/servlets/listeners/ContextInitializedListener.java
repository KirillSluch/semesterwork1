package ru.itis.servlets.listeners;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.jdbc.PostgresConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebListener
public class ContextInitializedListener implements ServletContextListener, HttpSessionListener, HttpSessionAttributeListener {

    public ContextInitializedListener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        final String SQL_CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS sw1.users (\n" +
                "    email varchar(25) primary key,\n" +
                "    name varchar(20),\n" +
                "    password varchar(16),\n" +
                "    access varchar(10)\n" +
                ");";

        //language=SQL
        final String SQL_CREATE_PRODUCTS_TABLE = "create table if not exists sw1.product (\n" +
                "    id bigserial,\n" +
                "    name varchar(20),\n" +
                "    description varchar(300),\n" +
                "    price float4,\n" +
                "    path_to_logo varchar(150),\n" +
                "    PRIMARY KEY (id)\n" +
                ");";

        //language=SQL
        final String SQL_CREATE_BASKET_TABLE = "create table if not exists sw1.basket (\n" +
                "    user_id varchar(25),\n" +
                "    product_id bigint,\n" +
                "   foreign key (user_id) REFERENCES sw1.users (email)," +
                "   foreign key (product_id) references sw1.product (id)" +
                ");";
        //language=SQL
        final String SQL_CREATE_ORDER_TABLE = "create table if not exists sw1.orders (\n" +
                "id bigserial primary key,\n" +
                "    user_id varchar(30),\n" +
                "    status varchar(10)," +
                "   foreign key (user_id) references sw1.users (email)" +
                ");" +
                "create table if not exists sw1.order_product (\n" +
                "   order_id bigint,\n" +
                "   product_id bigint,\n" +
                "    foreign key (order_id) references sw1.orders (id)," +
                "    foreign key (product_id) references sw1.product (id)" +
                ");";

        try (Connection connection = PostgresConnectionProvider.getConnection();
             PreparedStatement statementTable = connection.prepareStatement(SQL_CREATE_USERS_TABLE);
             PreparedStatement statementBasket = connection.prepareStatement(SQL_CREATE_BASKET_TABLE);
             PreparedStatement statementOrder = connection.prepareStatement(SQL_CREATE_ORDER_TABLE);
             PreparedStatement statementProduct = connection.prepareStatement(SQL_CREATE_PRODUCTS_TABLE)) {

            statementProduct.execute();
            statementTable.execute();
            statementBasket.execute();
            statementOrder.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
