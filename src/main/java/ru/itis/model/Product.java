package ru.itis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Product{
    public static List<String> getMeta() {
        List<String> metaList = new ArrayList<>();

        metaList.add("id");
        metaList.add("name");
        metaList.add("description");
        metaList.add("price");

        return metaList;
    }
    Long id;
    String name;
    String description;
    Float price;
    String pathToLogo;
}
