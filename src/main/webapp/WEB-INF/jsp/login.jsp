<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 10.10.2022
  Time: 19:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>
<html>
<head>
    <title>Log In</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<style>
    body {
        padding-top: 40px;
    }
</style>
<body>
    <c:if test="${sessionScope.errors != null}">
        <h2 class="mx-auto text-center">${sessionScope.errors}</h2>
        <c:set var="errors" scope="session" value="${null}" />
    </c:if>
    <div class="container-md w-25 mx-auto">
        <form method="post" action="">
            <div class="input-group mb-3">
                <input type="email" class="form-control" name="email"
                       aria-describedby="emailHelp" placeholder="Адресс вашей почты"/>
            </div>
            <div class="input-group mb-3">
                <input type="password" class="form-control" id="exampleInputPassword1"
                       name="password" placeholder="Пароль">
            </div>
            <button type="submit" class="btn btn-primary">Log In</button>
        </form>
        <form action="${pageContext.request.contextPath}/register" method="get">
            <label>
                <button type="submit" class="btn btn-primary">Registration</button>
                <div id="emailHelp" class="form-text">Здесь вы можете зарегистрироваться, если еще не сделали этого</div>
            </label>
        </form>
    </div>

</body>
</html>
