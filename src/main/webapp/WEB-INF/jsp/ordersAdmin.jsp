<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 20.10.2022
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Редактирование активных заказов</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <c:set var="orders" scope="session" value="${sessionScope.activeAdmin}"/>

    <c:set var="nameOfOrders" scope="session" value="Активные заказы"/>
    <h2>${nameOfOrders}</h2>
    <c:if test="${orders != null}">
        <c:forEach var="order" items="${orders.keySet()}">

            <h5>Заказ номер ${order}</h5>
            <table>
                <c:set var="total" scope="page" value="${0}"/>
                <c:forEach var="entry" items="${orders.get(order).entrySet()}">
                    <c:set var="product" scope="session" value="${entry.getKey()}"/>
                    <tr>
                        <td>
                                ${product.getName()}
                        </td>
                        <td>
                                ${product.getPrice() * entry.getValue()}
                            <c:set var="total" scope="page" value="${total + product.getPrice() * entry.getValue()}"/>
                        </td>
                        <td>
                                ${entry.getValue()}
                        </td>
                    </tr>

                </c:forEach>
                <tr>
                    <td>
                        <form method="post">
                            <button name="idToHistory" value="${order}" type="submit" class="btn btn-primary">Заказ выполнен</button>
                        </form>
                    </td>
                    <td></td>
                    <td>${total}</td>
                </tr>
            </table>
        </c:forEach>
    </c:if>
    <form method="get" action="${pageContext.request.contextPath}/shop/admin">
        <button type="submit" class="btn btn-primary">Вернуться в главное меню Администротора</button>
    </form>
</body>
</html>
