<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 20.10.2022
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    <jsp:include page="/css/index.css"/>
    body {
        padding-top: 40px;
    }
</style>
<header style="border: bisque;">
    <c:set var="user" scope="session" value="${sessionScope.user}"/>
    <c:set var="ADMIN" scope="page" value="ADMIN"/>
    <c:set var="path" scope="page" value="/SemestrWork1/shop/basket"/>

    <div class="d-flex justify-content-center fixed-top">
        <form action="${pageContext.request.contextPath}/shop/store">
            <button type="submit" class="btn btn-primary">Магазин</button>
        </form>

        <form action="${pageContext.request.contextPath}/shop/basket">
            <button type="submit" class="btn btn-primary">Корзина</button>
        </form>


        <form action="${pageContext.request.contextPath}/shop/store/orderMonitor">
            <button type="submit" class="btn btn-primary">Мои заказы</button>
        </form>


        <form action="${pageContext.request.contextPath}/login">
            <input type="hidden" value="drop" name="check">
            <button type="submit" class="btn btn-primary">Выход</button>
        </form>

        <c:if test="${user.getAccess().equals(ADMIN)}">
            <form action="${pageContext.request.contextPath}/shop/admin">
                <button type="submit" class="btn btn-danger">Редактирование товаров</button>
            </form>
        </c:if>

    </div>

</header>
