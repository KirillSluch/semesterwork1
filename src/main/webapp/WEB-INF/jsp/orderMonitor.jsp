<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 19.10.2022
  Time: 19:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Монитор Заказов</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<body>

    <jsp:include page="header.jsp"/>

    <div class="row align-items-start">
        <div class="col">
            <c:set var="orders" scope="session" value="${sessionScope.active}"/>
            <c:set var="nameOfOrders" scope="session" value="Активные заказы"/>
            <jsp:include page="orderPresenter.jsp"/>
        </div>
        <div class="col">
            <c:set var="orders" scope="session" value="${sessionScope.history}"/>
            <c:set var="nameOfOrders" scope="session" value="История"/>
            <jsp:include page="orderPresenter.jsp"/>
        </div>
    </div>

</body>
</html>
