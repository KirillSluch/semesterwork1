<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 11.10.2022
  Time: 19:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    <jsp:include page="/css/index.css"/>
    body {
        padding-top: 30px;
    }
</style>
<html>
<head>
    <title>Store</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<body id="store">

    <c:set var="user" scope="session" value="${sessionScope.user}"/>
    <c:set var="ADMIN" scope="page" value="ADMIN"/>
    <c:set var="path" scope="page" value="/SemestrWork1/shop/basket"/>

    <div class="d-flex justify-content-center fixed-top">
        <form action="${pageContext.request.contextPath}/shop/store">
            <button type="submit" class="btn btn-primary">Магазин</button>
        </form>

        <form method="post">
            <input hidden class="hidden" type="text" name="inOrder[]" value="${""}">
            <button id="basket" type="submit" class="btn btn-primary">Корзина</button>
        </form>

        <form action="${pageContext.request.contextPath}/shop/store/orderMonitor">
            <button type="submit" class="btn btn-primary">Мои заказы</button>
        </form>

        <form action="${pageContext.request.contextPath}/login">
            <input type="hidden" value="drop" name="check">
            <button type="submit" class="btn btn-primary">Выход</button>
        </form>

        <c:if test="${user.getAccess().equals(ADMIN)}">
            <form action="${pageContext.request.contextPath}/shop/admin">
                <button type="submit" class="btn btn-danger">Редактирование товаров</button>
            </form>
        </c:if>

    </div>


    <h1 class="text-center">Добро пожаловать, ${sessionScope.user.getName()}</h1>
    <c:if test="${sessionScope.errors != null}">
        <h2>${sessionScope.errors}</h2>
        <c:set var="errors" scope="session" value="${null}" />
    </c:if>

    <c:set var="productList" scope="session" value="${sessionScope.productList}"/>
    <div class="container-md text-center mx-auto">
    <c:if test="${productList != null}">
            <c:forEach var="product" items="${productList}">
                <div class="col">
                    <div class="card mx-auto" style="width: 30rem;" >
                        <img src="${pageContext.request.contextPath}/images/${product.getPathToLogo()}"
                             class="card-img-top" alt="ЛОГО">
                        <div class="card-body">
                            <h5 class="card-title">${product.getName()}</h5>
                            <p class="card-text">${product.getDescription()}</p>
                            <button class="appender btn btn-primary" type="button"
                                    name="toOrder" value="${product.getId()}">Добавить в корзину</button>
                            <p class="card-footer">Цена: ${product.getPrice()}</p>
                        </div>
                    </div>
                </div>
            </c:forEach>
    </c:if>


    <h5 class="p-3 mb-2 bg-info text-dark">Примеры наших работ</h5>

    <iframe width="853" height="480" src="https://www.youtube.com/embed/yWXahPUJP0k"
            title="Водное поло. 2 тур Чемпионата России. КОС-СИНТЕЗ - МК СШОР 'Восток' День 2-й."
            frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
    </div>
</body>
</html>

<script type="text/javascript">
    <jsp:include page="/js/jquery-3.6.1.min.js"/>
</script>

<script>
    $(".appender").click(function (e) {
        $('.hidden').val($('.hidden').val() + ' ' + e.target.value);
        console.log($(".hidden").get("value"))
    });

    $(window).bind("destroy",function(){
        $('.hidden').val("");
    });
    document.ready(() => {
        $('.hidden').val("");
    });



</script>