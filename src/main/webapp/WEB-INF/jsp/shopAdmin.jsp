<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 12.10.2022
  Time: 19:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>
<html>
<head>
    <title>Shop Admin</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<body>
    <jsp:include page="header.jsp"/>

    <h1 class="text-center">Добро пожаловать, админ ${sessionScope.user.getName()}</h1>

    <p>На этой страничке вы можете редактировать список продуктов на сайте</p>
    <c:set var="productList" value="${sessionScope.productList}" />
    <c:set var="metaList" value="${sessionScope.metaList}" />
    <c:set var="id" value="id" />

    <form action="" method="post">
        <table>
            <tr>
                <td>

                </td>
                <c:if test="${productList != null}">
                    <c:forEach var="string" items="${metaList}">
                        <th>${string}</th>
                    </c:forEach>
                </c:if>
            </tr>
            <c:if test="${productList != null}">
                <c:forEach var="product" items="${productList}">
                    <tr>
                        <td>
                            <input class="checkbox" type="checkbox" name="delete[]" value="${Long.parseLong(product.getId())}">
                        </td>
                        <td>
                                ${product.getId()}
                        </td>
                        <td>
                                <input id="name${product.getId()}" type="hidden" value="${product.getName()}">
                                ${product.getName()}
                        </td>
                        <td>
                                ${product.getDescription()}
                                <input id="description${product.getId()}" type="hidden"
                                       value="${product.getDescription()}">
                        </td>
                        <td>
                                ${product.getPrice()}
                                <input id="price${product.getId()}" type="hidden" value="${product.getPrice()}">
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
        </table>
        <button type="submit" class="btn btn-primary">Delete</button>
    </form>
    <div class="card" style="width: 40rem;">
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <label>
                    <input type="text" name="name" placeholder="Название продукта" required>
                </label>
                <label>
                    <input type="text" name="description"placeholder="Описание продукта" required>
                </label>
                <label>
                    <input type="number" name="price" placeholder="Цена продукта" required>
                </label>
                <label>
                    Изображение товара
                    <input type="file" name="image" placeholder="Изображение товара" required>
                </label>
                <br>
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
    <div class="row">
    <div class="card" style="width: 30rem;">
        <div class="card-body col">
            <form action="" method="post" enctype="multipart/form-data">
                <select id="select" name="id" onclick="getId()" required>
                    <option selected disabled>Выберите товар для обновления</option>
                    <c:if test="${productList != null}">
                        <c:forEach var="product" items="${productList}">
                            <option value="${product.getId()}">${product.getName()}</option>
                        </c:forEach>
                    </c:if>
                </select>
                <label>
                    <input id="newName" type="text" name="name" placeholder="Название продукта" required>
                </label>
                <label>
                    <input id="newDescription" type="text" name="description"placeholder="Описание продукта" required>
                </label>
                <label>
                    <input id="newPrice" type="number" name="price" placeholder="Цена продукта" required>
                </label>
                <label>
                    Изображение товара
                    <input type="file" name="image" required>
                </label>
                <br>
                <button type="submit" name="update" class="btn btn-primary">Update</button>
            </form>
        </div>
        </div>
    </div>

    <div class="container-md col">
        <form method="get" action="${pageContext.request.contextPath}/shop/admin/user">
            <button type="submit" class="btn btn-primary">Редактирование пользователей</button>
        </form>

        <form method="get" action="${pageContext.request.contextPath}/shop/admin/orders">
            <button type="submit" class="btn btn-primary">Активные заказы</button>
        </form>
    </div>
</body>
</html>
<script type="text/javascript">
    <jsp:include page="/js/jquery-3.6.1.min.js"/>
</script>

<script>
    function getId() {
        let id = $("#select").val();
        let name = "#name" + id;
        let description = "#description" + id;
        let price = "#price" + id;
        $("#newName").val($(name).val());
        $("#newDescription").val($(description).val());
        $("#newPrice").val($(price).val());
        console.log($(price).val());
        console.log(price);
    }
</script>
