create table sw1.order (
    id bigserial primary key,
    user_id varchar(30),
    status varchar(10)
);

create table sw1.order_product (
   order_id varchar(30),
   product_id bigint
);
