create table if not exists sw1.product (
    id bigserial,
    name varchar(20),
    description varchar(300),
    price float4,
    PRIMARY KEY (id)
);
