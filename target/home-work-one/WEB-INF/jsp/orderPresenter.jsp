<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 20.10.2022
  Time: 17:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="align-content-center mx-auto">
    <h3 class="p-3 mb-2 bg-success text-white">${nameOfOrders}</h3>
</div>
<c:if test="${orders != null}">
    <c:forEach var="order" items="${orders.keySet()}">
        <h5>Заказ номер ${order}</h5>
        <table>
            <c:set var="total" scope="page" value="${0}"/>
            <c:forEach var="entry" items="${orders.get(order).entrySet()}">
                <c:set var="product" scope="session" value="${entry.getKey()}"/>
                <tr>
                    <td>
                            ${product.getName()}
                    </td>
                    <td>
                            ${product.getPrice() * entry.getValue()}
                        <c:set var="total" scope="page" value="${total + product.getPrice() * entry.getValue()}"/>
                    </td>
                    <td>
                            ${entry.getValue()}
                    </td>
                </tr>
            </c:forEach>
            <tr>
                <td></td>
                <td></td>
                <td>${total}</td>
            </tr>
        </table>
    </c:forEach>
</c:if>
