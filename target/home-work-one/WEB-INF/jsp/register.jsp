<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 10.10.2022
  Time: 19:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>
<html>
<head>

    <title>Registration</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<body>
    <c:if test="${sessionScope.errors != null}">
        <h2>${sessionScope.errors}</h2>
        <c:set var="errors" scope="session" value="${null}" />
    </c:if>
    <div class="container text-center">
    <div class="col-md-3" style="background-color: red">  </div>
    <div class="col-md-6 offset-md-3">

        <form method="post" action="">
            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Имя" name="name" aria-label="Имя" onkeyup="submitCheckLogin()">
                </div>
                <div class="col">
                    <input type="email" name="email" id = "email" class="form-control"
                           onkeyup="submitCheckLogin()" placeholder="Email" aria-label="Email"
                           pattern="^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$"/>
                </div>
            </div>
            <div class="row gy-5">
                <div class="col">
                    <input id="password" type="password" onkeyup="submitCheckLogin()"
                           name="password" class="form-control" placeholder="Пароль" aria-label="Пароль"/>
                </div>
                <div class="col">
                    <input id="confirmPassword" type="password" name="confirm_password"
                           onkeyup="submitCheckPass()" onfocusout="submitCheckLogin()"
                           class="form-control" placeholder="Пароль еще раз" aria-label="Пароль еще раз"/>
                </div>
            </div>
            <button id = "register" type="submit" class="btn btn-primary" disabled>Register</button>
            <div class="error text-bg-danger p-3"></div>
        </form>

    </div>
    <div class="col-md-3" style="background-color: red">  </div>
    </div>
</body>

<script type="text/javascript">
    <%@include file="/js/jquery-3.6.1.min.js"%>
</script>

<script type="text/javascript">
    let check = false;
    function submitCheckPass() {
        let password = $("#password").val();
        let confPassword = $("#confirmPassword").val();

        if(password !== confPassword) {
            $(".error").html("Пароли не совпадают!");
            $("#register").attr("disabled", "disabled");
        } else {
            $("#register").removeAttr("disabled");
            $(".error").html("");
        }

    }
    function submitCheckLogin() {
        let name = $("#name").val();
        let email = $("#email").val();
        let password = $("#password").val();
        if(name === "" || email === "" || password === "") {
            $(".error").html("Имя пользователя, email и пароль должны быть не пусты!");
            $("#register").attr("disabled", "disabled");

        } else {
            $("#register").removeAttr("disabled");
            $(".error").html("");
        }
    }
    // $("#confirmPassword").on("keyup", submitCheck());
</script>

</html>
