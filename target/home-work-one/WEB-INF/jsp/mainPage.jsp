<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 10.10.2022
  Time: 19:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main Page</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<body>
    <div class="d-flex justify-content-center" style="padding-top: 20%">
        <form action="${pageContext.request.contextPath}/register" method="get">
            <button type="submit" class="btn btn-primary">Registration</button>
        </form>

        <form action="${pageContext.request.contextPath}/login" method="get">
            <button type="submit" class="btn btn-primary">Lon In</button>
        </form>
    </div>
</body>
</html>
