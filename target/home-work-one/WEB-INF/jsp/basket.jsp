<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 18.10.2022
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Корзинка</title>
    <jsp:include page="/html/bootstrapInclude.html"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="card mx-auto" style="width: 30rem;">
    <div class="card-body">
    <h1 class="card-header">
        Состав трансляции:
    </h1>
    <c:set var="productMap" scope="session" value="${sessionScope.inBasketMap}"/>
    <c:set var="total" scope="page" value="${0}"/>
    <c:if test="${productMap != null}">
        <table>
            <c:forEach var="entry" items="${productMap.entrySet()}">
                <c:set var="product" scope="session" value="${entry.getKey()}"/>
                <tr>
                    <td>
                            ${product.getName()}
                    </td>
                    <td>
                            ${product.getDescription()}
                    </td>
                    <td>
                            ${product.getPrice() * entry.getValue()}
                        <c:set var="total" scope="page" value="${total + product.getPrice() * entry.getValue()}"/>
                    </td>
                    <td>
                            ${entry.getValue()}
                    </td>
                    <td>
                        <form method="post">
                            <button type="submit" class="btn btn-primary" name="productToDelete" value="${product.getId()}">Удалить</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <h3 class="card-footer">
        Итого: ${total}
        <form method="post">
            <button type="submit" class="btn btn-primary">Заказать</button>
        </form>
    </h3>
</div>
</div>
</body>
</html>